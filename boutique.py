# boutique.py
boutique_articles = {
    1: {"identifiant": 1, "prix_unitaire": 1000, "stock_disponible": 20, "stock_limite": 5},
    2: {"identifiant": 2, "prix_unitaire": 1500, "stock_disponible": 15, "stock_limite": 3},
    3: {"identifiant": 3, "prix_unitaire": 500, "stock_disponible": 43, "stock_limite": 8},
    4: {"identifiant": 4, "prix_unitaire": 250, "stock_disponible": 38, "stock_limite": 7},
    5: {"identifiant": 5, "prix_unitaire": 300, "stock_disponible": 12, "stock_limite": 2},
    6: {"identifiant": 6, "prix_unitaire": 5000, "stock_disponible": 24, "stock_limite": 6},
    7: {"identifiant": 7, "prix_unitaire": 3500, "stock_disponible": 17, "stock_limite": 4},
    8: {"identifiant": 8, "prix_unitaire": 100, "stock_disponible": 50, "stock_limite": 10},
    9: {"identifiant": 9, "prix_unitaire": 4000, "stock_disponible": 31, "stock_limite": 9},
    10: {"identifiant": 10, "prix_unitaire": 2500, "stock_disponible": 36, "stock_limite": 6},
}


def vendre(article_id, quantite):
    article = boutique_articles.get(article_id)
    if article:
        if article["stock_disponible"] - quantite >= article["stock_limite"]:
            article["stock_disponible"] -= quantite
            print(f"Vente de {quantite} unités de l'article {article_id}.")
        else:
            print("Stock insuffisant.")
    else:
        print("Article non trouvé.")


def approvisionner(article_id, quantite):
    article = boutique_articles.get(article_id)
    if article:
        article["stock_disponible"] += quantite
        print(f"Approvisionnement de {quantite} unités de l'article {article_id}.")
    else:
        print("Article non trouvé.")


def consulter(article_id):
    article = boutique_articles.get(article_id)
    if article:
        print(
            f"Article {article_id}: Prix unitaire - {article['prix_unitaire']}, Stock disponible - {article['stock_disponible']}, Stock limite - {article['stock_limite']}.")
    else:
        print("Article non trouvé.")

    return article


def facture_total(achats):
    total = sum(boutique_articles[article_id]["prix_unitaire"] * quantite for article_id, quantite in achats.items())
    print(f"Le total des achats est de {total} euros.")
