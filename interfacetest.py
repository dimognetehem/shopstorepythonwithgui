from customtkinter import *
from CTkTable import CTkTable
from PIL import Image
from boutique import *


def vendre_ui():
    try:
        global articles_vendus
        selected_article_id = int(radio_var.get())
        # Mettre à jour le texte du label_stock_disponible en fonction de l'article sélectionné
        article_info = boutique_articles.get(selected_article_id)

        article_id = int(radio_var.get())
        quantite = int(entry_quantite.get())
        if article_info:
            if (quantite > 0):
                if article_info["stock_disponible"] - quantite >= article_info["stock_limite"]:
                    # article_info["stock_disponible"] -= quantite
                    vendre(article_id, quantite)
                    # Mettre à jour les données après une vente
                    for article in donnees:
                        if article[0] == article_id:
                            article[2] += quantite  # Mettre à jour la quantité vendue
                            article[3] += article[1] * quantite  # Mettre à jour le prix total dépensé
                            break

                    print(donnees)
                    global tabtest
                    donnees_filtrees = [ligne for ligne in donnees if all(element != 0 for element in ligne)]
                    articles_vendus += int(quantite)
                    afficher_message(f"Vente de {quantite} unités de l'article {article_id}")
                    label_art_vendus.configure(text=f"{articles_vendus}")

                else:
                    afficher_message("Stock insuffisant. Veuillez approvisionner")

            else:
                afficher_message("Veuillez entrer une quantité valide.")

            selectionner_article()
        else:
            afficher_message("Veuillez sélectionner un article d'abord.")
    except ValueError:
        # Gérer le cas où l'entrée n'est pas un entier valide
        afficher_message("Veuillez entrer une quantité valide.")


def approvisionner_ui():
    # article_id = int(radio_var.get())
    # quantite = int(entry_quantite_appro.get())
    # approvisionner(article_id, quantite)

    try:
        selected_article_id = int(radio_var.get())
        # Mettre à jour le texte du label_stock_disponible en fonction de l'article sélectionné
        article_info = boutique_articles.get(selected_article_id)

        article_id = int(radio_var.get())
        quantite = int(entry_quantite_appro.get())
        if article_info:
            if quantite > 0:
                # article_info["stock_disponible"] -= quantite
                approvisionner(article_id, quantite)
                afficher_message(f"Approvisionnement de {quantite} unités de l'article {article_id}")

            else:
                afficher_message("Veuillez entrer une quantité valide.")

            selectionner_article()
        else:
            afficher_message("Veuillez sélectionner un article d'abord.")
    except ValueError:
        # Gérer le cas où l'entrée n'est pas un entier valide
        afficher_message("Veuillez entrer une quantité valide.")


def consulter_ui():
    article_id = int(radio_var.get())
    article = consulter(article_id)
    if article:
        label_display = (CTkLabel(master=display_frame,
                                  text=f"Article ID : {article_id}   |   Prix unitaire : {article['prix_unitaire']} FCFA   |   Stock disponible : {article['stock_disponible']} unités   |   Stock limite : {article['stock_limite']} unités",
                                  font=("Arial Black", 13), text_color="#000"))
        label_display.grid(row=0, column=0, padx=27, pady=10)

    else:
        afficher_message("Veuillez sélectionner un article d'abord.")


def afficher_facture_total_ui():
    donnees_filtrees = [ligne for ligne in donnees if all(element != 0 for element in ligne)]
    print(donnees_filtrees)

    if not donnees_filtrees:

        label_message1.grid(row=0, column=2, padx=27)

    else:

        label_message1.grid_forget()
        tableau_final = [table_entete]
        for article in donnees_filtrees:
            tableau_final.append(article)

        # Calculer les totaux
        quantite_totale = sum(article[2] for article in tableau_final[1:])
        prix_total = sum(article[3] for article in tableau_final[1:])


        # Créer la nouvelle ligne pour les totaux
        ligne_totaux = ["Totaux", "", quantite_totale, prix_total]

        # Ajouter la nouvelle ligne à la fin de tableau_final
        tableau_final.append(ligne_totaux)

        if hasattr(app, 'table'):
            app.table.pack_forget()

        if hasattr(app, 'label_message2'):
            app.label_message2.pack_forget()

        app.table = CTkTable(master=table_frame, values=tableau_final, colors=["#E6E6E6", "#EEEEEE"],
                             header_color="#2A8C55", hover_color="#B4B4B4", font=("Arial Black", 13))
        print(tableau_final)

        app.table.edit_row(0, text_color="#fff", hover_color="#2A8C55")
        app.table.pack(expand=True, fill="both", padx=5, pady=(20, 15))

        app.label_message2 = CTkLabel(master=table_frame,
                                      text=f"Le montant total à payer est de {prix_total} FCFA !",
                                      text_color="#2a8c55", font=("Arial Black", 15), pady=20)
        app.label_message2.pack(side="bottom")


def afficher_message(message):
    label_message.configure(text=message)


def selectionner_article():
    selected_article_id = int(radio_var.get())

    # Mettre à jour le texte du label_stock_disponible en fonction de l'article sélectionné
    article_info = boutique_articles.get(selected_article_id)
    if article_info:
        stock_disponible = article_info.get("stock_disponible", 20)
        label_stock.configure(text=f"{stock_disponible}")
        # consulter(selected_article_id)  # Vous pouvez appeler consulter() ici si nécessaire


app = CTk()
app.title("Application de gestion d'une boutique")
app.geometry("1525x750")
app.resizable(0, 0)
articles_vendus = 0
set_appearance_mode("light")

sidebar_frame = CTkFrame(master=app, fg_color="#2A8C55", width=176, height=650, corner_radius=0)
sidebar_frame.pack_propagate(0)
sidebar_frame.pack(fill="y", anchor="w", side="left")

logo_img_data = Image.open("logo.png")
logo_img = CTkImage(dark_image=logo_img_data, light_image=logo_img_data, size=(77.68, 85.42))

CTkLabel(master=sidebar_frame, text="", image=logo_img).pack(pady=(38, 0), anchor="center")

package_img_data = Image.open("package_icon.png")
package_img = CTkImage(dark_image=package_img_data, light_image=package_img_data)

CTkButton(master=sidebar_frame, image=package_img, text="Accueil", fg_color="#fff", font=("Arial Bold", 14),
          text_color="#2A8C55", hover_color="#eee", anchor="w").pack(anchor="center", ipady=5, pady=(75, 0))

settings_img_data = Image.open("settings_icon.png")
settings_img = CTkImage(dark_image=settings_img_data, light_image=settings_img_data)
CTkButton(master=sidebar_frame, image=settings_img, text="Settings", fg_color="transparent", font=("Arial Bold", 14),
          hover_color="#207244", anchor="w").pack(anchor="center", ipady=5, pady=(325, 0))

main_view = CTkFrame(master=app, fg_color="#fff", width=1500, height=800, corner_radius=0)
main_view.pack_propagate(0)
main_view.pack(side="left")

title_frame = CTkFrame(master=main_view, fg_color="transparent")
title_frame.pack(anchor="n", fill="x", padx=27, pady=(29, 0))

CTkLabel(master=title_frame, text="Bienvenue dans notre boutique", font=("Arial Black", 25), text_color="#2A8C55").pack(
    anchor="nw", side="left")

metrics_frame = CTkFrame(master=main_view, fg_color="transparent")
metrics_frame.pack(anchor="n", fill="x", padx=27, pady=(36, 0))

orders_metric = CTkFrame(master=metrics_frame, fg_color="#2A8C55", width=250, height=65)
orders_metric.grid_propagate(0)
orders_metric.pack(side="left")

logitics_img_data = Image.open("logistics_icon.png")
logistics_img = CTkImage(light_image=logitics_img_data, dark_image=logitics_img_data, size=(43, 43))

CTkLabel(master=orders_metric, image=logistics_img, text="").grid(row=0, column=0, rowspan=2, padx=(12, 5), pady=10)

CTkLabel(master=orders_metric, text="Nombre d'articles", text_color="#fff", font=("Arial Black", 15)).grid(row=0,
                                                                                                           column=1,
                                                                                                           sticky="sw")
CTkLabel(master=orders_metric, text="10", text_color="#fff", font=("Arial Black", 25), justify="left").grid(row=1,
                                                                                                            column=1,
                                                                                                            sticky="nw",
                                                                                                            pady=(
                                                                                                                0, 10))

shipped_metric = CTkFrame(master=metrics_frame, fg_color="#2A8C55", width=250, height=65)
shipped_metric.grid_propagate(0)
shipped_metric.pack(side="left", expand=True, anchor="center")

shipping_img_data = Image.open("shipping_icon.png")
shipping_img = CTkImage(light_image=shipping_img_data, dark_image=shipping_img_data, size=(43, 43))

CTkLabel(master=shipped_metric, image=shipping_img, text="").grid(row=0, column=0, rowspan=2, padx=(12, 5), pady=10)

CTkLabel(master=shipped_metric, text="Stock disponible", text_color="#fff", font=("Arial Black", 15)).grid(row=0,
                                                                                                           column=1,
                                                                                                           sticky="sw")
label_stock = CTkLabel(master=shipped_metric, text="20", text_color="#fff", font=("Arial Black", 25), justify="left")
label_stock.grid(row=1, column=1, sticky="nw", pady=(0, 10))

delivered_metric = CTkFrame(master=metrics_frame, fg_color="#2A8C55", width=250, height=65)
delivered_metric.grid_propagate(0)
delivered_metric.pack(side="right", )

delivered_img_data = Image.open("delivered_icon.png")
delivered_img = CTkImage(light_image=delivered_img_data, dark_image=delivered_img_data, size=(43, 43))

CTkLabel(master=delivered_metric, image=delivered_img, text="").grid(row=0, column=0, rowspan=2, padx=(12, 5), pady=10)

CTkLabel(master=delivered_metric, text="Articles vendus", text_color="#fff", font=("Arial Black", 15)).grid(row=0,
                                                                                                            column=1,
                                                                                                            sticky="sw")
label_art_vendus = (
    CTkLabel(master=delivered_metric, text="0", text_color="#fff", font=("Arial Black", 25), justify="left"))
label_art_vendus.grid(row=1, column=1, sticky="nw", pady=(0, 10))

table_entete = ["Article ID", "Prix Unitaire", "Quantité", "Total"]

donnees = [
    [1, 1000, 0, 0],
    [2, 1500, 0, 0],
    [3, 500, 0, 0],
    [4, 250, 0, 0],
    [5, 300, 0, 0],
    [6, 5000, 0, 0],
    [7, 3500, 0, 0],
    [8, 100, 0, 0],
    [9, 4000, 0, 0],
    [10, 2500, 0, 0]
]

print("Données")
print(donnees)
donnees_filtrees = [ligne for ligne in donnees if all(element != 0 for element in ligne)]
print("Filtrees")
print(donnees_filtrees)

# Afficher le résultat
# for ligne in donnees_filtrees:
#     print(ligne)

# Créer une liste de listes
tableau_final = [table_entete]  # La première liste contient les entêtes

# Ajouter les données sous les entêtes appropriées

# Afficher le résultat
# for ligne in tableau_final:
#     print(ligne)
# table_data = [
#     ["Article ID", "Prix Unitaire", "Quantité", "Total"],
#
# ]

radio_var = StringVar(app)
radio_var.set("1")  # Définir l'article sélectionné par défaut

radiobutton_frame = CTkFrame(master=main_view, width=200, height=60, fg_color="#EEEEEE", corner_radius=8)
radiobutton_frame.pack(side="left", fill="both", padx=27, pady=(36, 20))
label_radio_group = CTkLabel(master=radiobutton_frame, text="Veuillez sélectionner un article : ", text_color="#000")
label_radio_group.grid(row=0, column=2, columnspan=1, padx=10, pady=10, sticky="")
radio_var = IntVar(value=0)
radio_button_1 = CTkRadioButton(text="Article 1", font=("Arial Black", 13), master=radiobutton_frame,
                                variable=radio_var, value=1, command=selectionner_article)
radio_button_1.grid(row=1, column=2, pady=10, padx=20, sticky="n")
radio_button_2 = CTkRadioButton(text="Article 2", font=("Arial Black", 13), master=radiobutton_frame,
                                variable=radio_var, value=2, command=selectionner_article)
radio_button_2.grid(row=2, column=2, pady=10, padx=20, sticky="n")
radio_button_3 = CTkRadioButton(text="Article 3", font=("Arial Black", 13), master=radiobutton_frame,
                                variable=radio_var, value=3, command=selectionner_article)
radio_button_3.grid(row=3, column=2, pady=10, padx=20, sticky="n")
radio_button_4 = CTkRadioButton(text="Article 4", font=("Arial Black", 13), master=radiobutton_frame,
                                variable=radio_var, value=4, command=selectionner_article)
radio_button_4.grid(row=4, column=2, pady=10, padx=20, sticky="n")
radio_button_5 = CTkRadioButton(text="Article 5", font=("Arial Black", 13), master=radiobutton_frame,
                                variable=radio_var, value=5, command=selectionner_article)
radio_button_5.grid(row=5, column=2, pady=10, padx=20, sticky="n")
radio_button_6 = CTkRadioButton(text="Article 6", font=("Arial Black", 13), master=radiobutton_frame,
                                variable=radio_var, value=6, command=selectionner_article)
radio_button_6.grid(row=6, column=2, pady=10, padx=20, sticky="n")
radio_button_7 = CTkRadioButton(text="Article 7", font=("Arial Black", 13), master=radiobutton_frame,
                                variable=radio_var, value=7, command=selectionner_article)
radio_button_7.grid(row=7, column=2, pady=10, padx=20, sticky="n")
radio_button_8 = CTkRadioButton(text="Article 8", font=("Arial Black", 13), master=radiobutton_frame,
                                variable=radio_var, value=8, command=selectionner_article)
radio_button_8.grid(row=8, column=2, pady=10, padx=20, sticky="n")
radio_button_9 = CTkRadioButton(text="Article 9", font=("Arial Black", 13), master=radiobutton_frame,
                                variable=radio_var, value=9, command=selectionner_article)
radio_button_9.grid(row=9, column=2, pady=10, padx=20, sticky="n")
radio_button_10 = CTkRadioButton(text="Article 10", font=("Arial Black", 13), master=radiobutton_frame,
                                 variable=radio_var, value=10, command=selectionner_article)
radio_button_10.grid(row=10, column=2, pady=10, padx=20, sticky="n")

input_frame = CTkFrame(master=main_view, width=200, height=60, fg_color="#EEEEEE", corner_radius=8)
input_frame.pack(side="left", fill="both", padx=10, pady=(36, 20))
label_message = CTkLabel(master=input_frame, text="", text_color="#e81123")
label_message.grid(row=0, column=2, padx=27)

label_souhaitez_vendre = CTkLabel(master=input_frame, text="Souhaitez-vous vendre ?")
entry_quantite = CTkEntry(master=input_frame)
btn_vendre = CTkButton(master=input_frame, fg_color="#2A8C55", text="Vendre", font=("Arial Black", 13),
                       hover_color="#1a5936", command=vendre_ui)
label_approvisionnement = CTkLabel(master=input_frame, text="Souhaitez-vous approvisionner ?")
entry_quantite_appro = CTkEntry(master=input_frame)
btn_approvisionner = CTkButton(master=input_frame, fg_color="#2A8C55", font=("Arial Black", 13), hover_color="#1a5936",
                               text="Approvisionner", command=approvisionner_ui)
label_consultation = CTkLabel(master=input_frame, text="Consulter l'article")
label_facture = CTkLabel(master=input_frame, text="Générer la facture totale")

label_souhaitez_vendre.grid(row=1, column=2, padx=(0, 10), pady=(10, 0))
entry_quantite.grid(row=2, column=2, padx=(10, 20), pady=(0, 10))
btn_vendre.grid(row=3, column=2, padx=(0, 10), pady=(0, 30))

label_approvisionnement.grid(row=4, column=2, padx=(6, 10), pady=(10, 0))
entry_quantite_appro.grid(row=5, column=2, padx=(10, 20), pady=(0, 10))
btn_approvisionner.grid(row=6, column=2, padx=(0, 10), pady=(0, 30))

label_consultation.grid(row=7, column=2, padx=(0, 10), pady=(10, 0))
label_facture.grid(row=9, column=2, padx=(0, 10), pady=(10, 0))
btn_consulter = CTkButton(master=input_frame, fg_color="#2A8C55", font=("Arial Black", 13), hover_color="#1a5936",
                          text="Consulter", command=consulter_ui)
btn_consulter.grid(row=8, column=2, padx=(0, 10), pady=(0, 30))
btn_facture_total = CTkButton(master=input_frame, fg_color="#2A8C55", font=("Arial Black", 13), hover_color="#1a5936",
                              text="Facture Totale", command=afficher_facture_total_ui)

btn_facture_total.grid(row=10, column=2, padx=(0, 10), pady=(0, 10))

display_frame = CTkFrame(master=main_view, width=200, height=80, fg_color="#EEEEEE", corner_radius=8)
display_frame.pack(fill="both", padx=10, pady=(36, 20))

table_frame = CTkScrollableFrame(master=main_view, fg_color="transparent")
table_frame.pack(expand=True, fill="both", padx=5, pady=(20, 15))
label_message1 = CTkLabel(master=table_frame,
                          text="Impossible de générer la facture, vous n'avez encore fait aucun achat !",
                          text_color="#e81123", font=("Arial Black", 20), pady=20)

app.mainloop()
