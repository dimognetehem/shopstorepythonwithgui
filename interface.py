# menu_principal.py
from customtkinter import *
from tkinter import Frame
from boutique import *


def vendre_ui():
    try:

        article_id = int(selected_article_id_var.get())
        quantite = int(entry_quantite.get())
        if(quantite > 0):
            vendre(article_id, quantite)
            afficher_message(f"Vente de {quantite} unités de l'article {article_id}")
        else:
            afficher_message("Veuillez entrer une quantité valide.")

        selectionner_article()
    except ValueError:
        # Gérer le cas où l'entrée n'est pas un entier valide
        afficher_message("Veuillez entrer une quantité valide.")


def approvisionner_ui():
    article_id = int(entry_article_id.get())
    quantite = int(entry_quantite.get())
    approvisionner(article_id, quantite)


def consulter_ui():
    article_id = int(entry_article_id.get())
    consulter(article_id)


def afficher_facture_total_ui():
    facture_total({})


def afficher_message(message):
    label_message.configure(text=message)




# selected_article_id = 1


# menu_principal.py
# ... (importations)

# def selectionner_article():
#     selected_article_id = int(selected_article.get())
#
#     # Mettre à jour le texte du label_stock_disponible en fonction de l'article sélectionné
#     article_info = boutique_articles.get(selected_article_id)
#     if article_info:
#         stock_disponible = article_info.get("stock_disponible", 0)
#         label_stock_disponible.configure(text=f"Stock disponible : {stock_disponible}")
#         consulter(selected_article_id)  # Vous pouvez appeler consulter() ici si nécessaire


# ... (le reste du code reste inchangé)

#
# # Créer la fenêtre principale
# root = CTk()
# root.title("Boutique Application")
#
# # Créer des widgets pour l'interface
# label_article_id = CTkLabel(root, text="Article ID:")
# entry_article_id = CTkEntry(root)
# label_quantite = CTkLabel(root, text="Quantité:")
# entry_quantite = CTkEntry(root)
#
# btn_vendre = CTkButton(root, text="Vendre", command=vendre_ui)
# btn_approvisionner = CTkButton(root, text="Approvisionner", command=approvisionner_ui)
# btn_consulter = CTkButton(root, text="Consulter", command=consulter_ui)
# btn_facture_total = CTkButton(root, text="Afficher Facture Totale", command=afficher_facture_total_ui())
#
# # Organiser les widgets dans la fenêtre
# label_article_id.grid(row=0, column=0)
# entry_article_id.grid(row=0, column=1)
# label_quantite.grid(row=1, column=0)
# entry_quantite.grid(row=1, column=1)
#
# btn_vendre.grid(row=2, column=0)
# btn_approvisionner.grid(row=2, column=1)
# btn_consulter.grid(row=2, column=2)
# btn_facture_total.grid(row=3, column=0, columnspan=3)
def selectionner_article():
    selected_article_id = int(selected_article_id_var.get())

    # Mettre à jour le texte du label_stock_disponible en fonction de l'article sélectionné
    article_info = boutique_articles.get(selected_article_id)
    if article_info:
        stock_disponible = article_info.get("stock_disponible", 20)
        label_stock_disponible.configure(text=f"Stock disponible : {stock_disponible}")
        # consulter(selected_article_id)  # Vous pouvez appeler consulter() ici si nécessaire


# Créer la fenêtre principale
root = CTk()
root.title("Boutique Application")
label_message = CTkLabel(root, text="")
label_message.grid(row=0, column=2, padx=(0, 10), pady=(0, 10))
# Créer des widgets pour l'interface
selected_article_id_var = StringVar(root)
selected_article_id_var.set("1")  # Définir l'article sélectionné par défaut
entry_article_id = CTkEntry(root)
# Simuler des boutons radio en utilisant une liste de RadioButton
radio_buttons = [CTkRadioButton(root, text=f"Article {article_id}", variable=selected_article_id_var, value=str(article_id),
                                command=selectionner_article) for article_id in boutique_articles.keys()]

label_stock_disponible = CTkLabel(root,
                                  text="Stock disponible : 20")  # Mettez à jour le texte en temps réel lors de la sélection d'un article


# ... (ajoutez d'autres widgets selon vos besoins)

# Organiser les widgets dans la fenêtre en utilisant la gestion de la grille


label_souhaitez_vendre = CTkLabel(root, text="Souhaitez-vous vendre ?")
entry_quantite = CTkEntry(root)
btn_vendre = CTkButton(root, text="Vendre", command=vendre_ui)
label_approvisionnement = CTkLabel(root, text="Souhaitez-vous approvisionner ?")
btn_approvisionner = CTkButton(root, text="Approvisionner", command=approvisionner_ui)

# Organiser les widgets dans la fenêtre en utilisant la gestion de la grille
for i, radio_button in enumerate(radio_buttons):
    radio_button.grid(row=i, column=0, padx=(10, 10), pady=(10, 0))

Frame(root, width=2, bg="black").grid(row=0, column=1, rowspan=len(radio_buttons), sticky='ns', padx=(0, 10),
                                      pady=(10, 0))

label_stock_disponible.grid(row=0, column=2, padx=(0, 10), pady=(10, 0))
label_souhaitez_vendre.grid(row=1, column=2, padx=(0, 10), pady=(10, 0))
entry_quantite.grid(row=2, column=2, padx=(10, 20), pady=(0, 10))
btn_vendre.grid(row=3, column=2, padx=(0, 10), pady=(0, 10))

label_approvisionnement.grid(row=4, column=2, padx=(0, 10), pady=(10, 0))
btn_approvisionner.grid(row=5, column=2, padx=(0, 10), pady=(0, 10))

Frame(root, width=2, bg="black").grid(row=0, column=3, rowspan=len(radio_buttons), sticky='ns', padx=(0, 10),
                                      pady=(10, 0))

# Lancer la boucle principale de customtkinter
root.mainloop()
